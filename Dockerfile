FROM openjdk:8-jdk-alpine
ARG JAR_FILE
COPY target/oxagile-eshop-trial-task-1.0-SNAPSHOT-exec.jar /oxagile-eshop-trial-task-1.0-SNAPSHOT-exec.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar","/oxagile-eshop-trial-task-1.0-SNAPSHOT-exec.jar"]