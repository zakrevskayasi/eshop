package com.oxagile.eshop.constants;

public interface CategoryConstants {

    int POPULAR_CATEGORIES_RATING = 5;

    String SELECT_CATEGORY_DROP_DOWN_MSG = "Выберите категорию...";

}

