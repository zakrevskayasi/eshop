package com.oxagile.eshop.constants;

public interface CommandsConstants {
    String COMMAND_REQUEST_PARAM = "command";
    String START_PAGE_COMMAND = "start_page";
    String REDIRECT_SIGNIN_COMMAND = "sign-in";
    String AUTH_COMMAND = "auth";
    String REDIRECT_SIGNUP_COMMAND = "signup-redirect";
    String SIGNUP_COMMAND = "signup";
    String SIGNOUT_COMMAND = "logout";
    String REDIRECT_CATEGORY_COMMAND = "category-redirect";
    String REDIRECT_PRODUCT_COMMAND = "product-redirect";
    String REDIRECT_SHOPPING_CART_COMMAND = "redirect-to-shopping-cart";
    String DELETE_PRODUCT_FROM_SHOPPING_CART_COMMAND = "delete-from-cart";
    String ADD_PRODUCT_TO_CART = "add-product-to-cart";
    String CREATE_ORDER_COMMAND = "create_order";
    String SEARCH_COMMAND = "search";

    String CMS_REDIRECT_COMMAND = "redirect-to-cms";
    String ADD_CATEGORY_FORM_REDIRECT_COMMAND = "redirect-add-new-category-form";
}
