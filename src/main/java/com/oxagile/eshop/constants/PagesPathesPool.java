package com.oxagile.eshop.constants;

public interface PagesPathesPool {

    String START_PAGE = "homepage";
    String SIGN_IN_PAGE = "login";
    String SIGN_UP_PAGE = "registration";
    String CABINET_PAGE = "cabinet";
    String PAGE_INDEX = "index.jsp";
    String CATEGORY_PAGE = "category";
    String PRODUCT_PAGE = "product";
    String CART_PAGE = "cart";
    String SEARCH_PAGE = "searchpage";

    String CMS_HOME_PAGE = "/cms/homePage.jsp";
    String ADD_NEW_CATEGORY_PAGE = "/cms/addCategory.jsp";

}
