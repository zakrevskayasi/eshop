package com.oxagile.eshop.constants;

public interface RequestParamsConstants {
    int DEFAULT_RECORDS_COUNT_PER_PAGE = 10;
    int DEFAULT_BALANCE = 1000;

    String LOGIN = "username";
    String PASS = "password";
    String SIGN_UP_NAME = "newUsrName";
    String SIGN_UP_SURNAME = "newUsrSurname";
    String SIGN_UP_EMAIL = "newUsrEmail";
    String SIGN_UP_PASS = "newUsrPass";
    String SIGN_UP_REPEATPASS = "repeatPass";
    String SIGN_UP_BRTHDAY = "brthDay";
    String SIGN_UP_BRTHMONTH = "brthMonth";
    String SIGN_UP_BRTHYEAR = "brthYear";

    String USER_DATA = "user";
    String USER_ORDERS = "ordersList";
    String POPULAR_CATEGORIES_LIST_REQ_PARAM = "categoriesList";
    String ALL_CATEGORIES_LIST_PARAM = "allCategoriesList";

    String CATEGORY_PARAM = "category";
    String CATEGORY_ID_PARAM = "category_id";
    String PRODUCT = "product";
    String SHOPPING_CART = "cart";
    String OPEN_SHOPPING_CART_AFTER_LOGIN = "redirectToCart";
    String PRODUCT_ID_PARAM = "product_id";
    String PRODUCT_RECORDS_PER_PAGE = "recordsPerPage";

    String NUMBER_OF_PRODUCT_PAGES = "numberOfPages";
    String PRODUCTS_LIST_CURRENT_PAGE = "currentPage";
    String FOUNDED_PRODUCTS_LIST = "searchResultList";

    String SEARCH_KEY = "searchKey";
    String CATEGORY_SEARCH_KEY = "categorySearchKey";
    String PRICE_FROM_KEY = "priceFromKey";
    String PRICE_TO_KEY = "priceToKey";

}
