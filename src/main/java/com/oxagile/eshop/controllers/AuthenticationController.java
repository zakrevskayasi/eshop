package com.oxagile.eshop.controllers;

import com.oxagile.eshop.entities.Order;
import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.OrderService;
import com.oxagile.eshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.oxagile.eshop.constants.PagesPathesPool.*;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Controller
@SessionAttributes({USER_DATA, OPEN_SHOPPING_CART_AFTER_LOGIN})
public class AuthenticationController {

    private final UserService userService;

    private final OrderService orderService;

    @Autowired
    public AuthenticationController(UserService userService, OrderService orderService) {
        this.userService = userService;
        this.orderService = orderService;
    }

    @RequestMapping("/cabinet")
    public ModelAndView openCabinet(@ModelAttribute(OPEN_SHOPPING_CART_AFTER_LOGIN) boolean openCart) throws ServiceException {
        ModelMap modelParams = new ModelMap();
        ModelAndView modelAndView;

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.getUserByEmail(username);
        List<Order> orders = orderService.getOrdersListForUser(user);

        modelParams.addAttribute(USER_DATA, user);
        modelParams.addAttribute(USER_ORDERS, orders);

        if (openCart) {
            modelAndView = new ModelAndView(CART_PAGE, modelParams);
        } else {
            modelAndView = new ModelAndView(CABINET_PAGE, modelParams);
        }

        return modelAndView;
    }

    @RequestMapping("/login")
    public ModelAndView login(@RequestParam(value = LOGIN, required = false) String username,
                              @RequestParam(value = PASS, required = false) String password,
                              @ModelAttribute(OPEN_SHOPPING_CART_AFTER_LOGIN) boolean openCart) throws ServiceException {
        ModelMap modelParams = new ModelMap();
        ModelAndView modelAndView;

        if (username != null && !username.isEmpty()) {
            User usr = userService.getUserByCreds(username, password);
            List<Order> orders = orderService.getOrdersListForUser(usr);

            modelParams.addAttribute(USER_DATA, usr);
            modelParams.addAttribute(USER_ORDERS, orders);

            if (openCart) {
                modelAndView = new ModelAndView(CART_PAGE, modelParams);
            } else {
                modelAndView = new ModelAndView(CABINET_PAGE, modelParams);
            }
        } else {
            modelAndView = new ModelAndView(SIGN_IN_PAGE);
        }

        return modelAndView;
    }

    @RequestMapping("/logout")
    public ModelAndView signOut(HttpSession session) {
        session.invalidate();
        return new ModelAndView(PAGE_INDEX);
    }

    @ModelAttribute(OPEN_SHOPPING_CART_AFTER_LOGIN)
    public boolean getDefaultRedirectToCartValue() {
        return false;
    }

    @ModelAttribute(USER_DATA)
    public User getEmptyUser() {
        return null;
    }

}
