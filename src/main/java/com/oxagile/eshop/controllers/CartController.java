package com.oxagile.eshop.controllers;

import com.oxagile.eshop.entities.Cart;
import com.oxagile.eshop.entities.Order;
import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.exceptions.ControllerException;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.OrderService;
import com.oxagile.eshop.services.ProductService;
import com.oxagile.eshop.services.UserService;
import com.oxagile.eshop.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.List;

import static com.oxagile.eshop.constants.PagesPathesPool.CABINET_PAGE;
import static com.oxagile.eshop.constants.PagesPathesPool.CART_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Controller
@SessionAttributes({SHOPPING_CART, USER_DATA, OPEN_SHOPPING_CART_AFTER_LOGIN})
public class CartController {

    private final ProductService productService;

    private final OrderService orderService;

    private final UserService userService;

    @Autowired
    public CartController(ProductService productService, OrderService orderService, UserService userService) {
        this.productService = productService;
        this.orderService = orderService;
        this.userService = userService;
    }

    @RequestMapping("/addProductToCart")
    public ModelAndView addProductToCart(@RequestParam(PRODUCT_ID_PARAM) String id, @ModelAttribute(SHOPPING_CART) Cart shopCart) throws ServiceException {
        ModelMap modelParams = new ModelMap();

        int productId = Integer.parseInt(id);
        Product product = productService.read(productId).get();
        shopCart.addProduct(product);

        modelParams.addAttribute(PRODUCT, product);
        modelParams.addAttribute(SHOPPING_CART, shopCart);

        return new ModelAndView(CART_PAGE, modelParams);
    }

    @RequestMapping("/deleteProductFromCart")
    public ModelAndView deleteProductFromCart(@RequestParam(PRODUCT_ID_PARAM) String id, @ModelAttribute(SHOPPING_CART) Cart shopCart) throws ControllerException {
        ModelMap modelParams = new ModelMap();

        if (shopCart == null) {
            throw new ControllerException("Shopping cart is not created");
        }

        int productId = Integer.parseInt(id);
        shopCart.removeProduct(productId);
        modelParams.addAttribute(SHOPPING_CART, shopCart);

        return new ModelAndView(SHOPPING_CART, modelParams);
    }

    @RequestMapping("/createOrder")
    public ModelAndView createOrder(@ModelAttribute(SHOPPING_CART) Cart cart) throws ServiceException {
        Order order;
        ModelMap modelParams = new ModelMap();
        ModelAndView modelAndView;

        List<Product> products = cart.getProducts();
        String date = DateUtils.formatLocalDateToString(LocalDate.now(), "dd-MM-yyyy");
        int totalPrice = cart.getTotalPrice();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = userService.getUserByEmail(username);

        if (products.size() != 0) {
            order = Order.builder().build();
            order.setUser(user);
            order.setPrice(totalPrice);
            order.setDate(date);
            order.setProductsList(products);
            orderService.create(order);
        }

        List<Order> orders = orderService.getOrdersListForUser(user);
        cart.clear();

        modelParams.addAttribute(USER_ORDERS, orders);
        modelParams.addAttribute(SHOPPING_CART, cart);

        modelAndView = new ModelAndView(CABINET_PAGE, modelParams);

        return modelAndView;
    }

    @RequestMapping("/openCart")
    public ModelAndView redirectToShoppingCart() {
        return new ModelAndView(CART_PAGE);
    }

    @ModelAttribute(USER_DATA)
    public User getEmptyUser() {
        return null;
    }

    @ModelAttribute(SHOPPING_CART)
    public Cart shoppingCart() {
        return new Cart();
    }
}
