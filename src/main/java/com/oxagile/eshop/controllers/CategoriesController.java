package com.oxagile.eshop.controllers;

import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CategoriesController {

    private final CategoryService categoryService;


    @Autowired
    public CategoriesController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping("/category")
    public ModelAndView categoryPageRedirect(@RequestParam("category_id") String id,
                                             @RequestParam("recordsPerPage") String recordsPerPage,
                                             @RequestParam("currentPage") String currentPageNum) throws ServiceException {

        return categoryService.getCategoryModel(id, recordsPerPage, currentPageNum);
    }

}
