package com.oxagile.eshop.controllers;

import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static com.oxagile.eshop.constants.PagesPathesPool.PRODUCT_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.PRODUCT;
import static com.oxagile.eshop.constants.RequestParamsConstants.PRODUCT_ID_PARAM;

@Controller
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping("/product")
    public ModelAndView redirectProductPage(@RequestParam(PRODUCT_ID_PARAM) String id) throws ServiceException {
        ModelMap model = new ModelMap();
        int productId = Integer.parseInt(id);

        Product product = productService.read(productId).get();
        model.addAttribute(PRODUCT, product);

        return new ModelAndView(PRODUCT_PAGE, model);
    }
}
