package com.oxagile.eshop.controllers;

import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Controller
public class SearchController {

    private final SearchService searchService;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @RequestMapping("/search")
    public ModelAndView search(HttpServletRequest request, @RequestParam(SEARCH_KEY) String searchKey,
                               @RequestParam(value = CATEGORY_SEARCH_KEY, required = false) String category,
                               @RequestParam(value = PRICE_FROM_KEY, required = false) String priceFrom,
                               @RequestParam(value = PRICE_TO_KEY, required = false) String priceTo,
                               @RequestParam(value = PRODUCT_RECORDS_PER_PAGE, required = false) String recordsPerPage,
                               @RequestParam(value = PRODUCTS_LIST_CURRENT_PAGE, required = false) String productsListCurrentPage) throws ServiceException {

        return searchService.search(request, searchKey, category, priceFrom, priceTo, recordsPerPage, productsListCurrentPage);
    }

}
