package com.oxagile.eshop.controllers;

import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import static com.oxagile.eshop.constants.PagesPathesPool.SIGN_UP_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Controller
@SessionAttributes(USER_DATA)
public class SignUpController {

    private final UserService userService;

    @Autowired
    public SignUpController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/openSignUpPage")
    public ModelAndView openSignUpPage() {
        return new ModelAndView(SIGN_UP_PAGE);
    }

    @RequestMapping("/sign-up")
    public ModelAndView signUp(@RequestParam(SIGN_UP_NAME) String username,
                               @RequestParam(SIGN_UP_SURNAME) String surname,
                               @RequestParam(SIGN_UP_BRTHDAY) String brthday,
                               @RequestParam(SIGN_UP_BRTHMONTH) String brthmonth,
                               @RequestParam(SIGN_UP_BRTHYEAR) String brthYear,
                               @RequestParam(SIGN_UP_EMAIL) String email,
                               @RequestParam(SIGN_UP_PASS) String password,
                               @RequestParam(SIGN_UP_REPEATPASS) String repeatPass) throws ServiceException {

        return userService.getNewUserModel(username, surname, brthday, brthmonth, brthYear, email, password, repeatPass);
    }

}
