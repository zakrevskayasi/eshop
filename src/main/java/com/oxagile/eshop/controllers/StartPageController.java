package com.oxagile.eshop.controllers;

import com.oxagile.eshop.entities.Category;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.oxagile.eshop.constants.PagesPathesPool.START_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Controller
public class StartPageController {

    private final CategoryService categoryService;

    @Autowired
    public StartPageController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @RequestMapping({"/home", "", "/", "home.html", "index.html"})
    public ModelAndView executeCommand() throws ServiceException {
        ModelMap model = new ModelMap();
        List<Category> categoriesList = categoryService.getAllCategories();

        model.addAttribute(POPULAR_CATEGORIES_LIST_REQ_PARAM, categoriesList);
        model.addAttribute(PRODUCT_RECORDS_PER_PAGE, DEFAULT_RECORDS_COUNT_PER_PAGE);

        return new ModelAndView(START_PAGE, model);
    }

}
