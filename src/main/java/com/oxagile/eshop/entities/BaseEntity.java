package com.oxagile.eshop.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@MappedSuperclass
abstract public class BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

}
