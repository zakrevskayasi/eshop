package com.oxagile.eshop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Builder
@Table(name = "CATEGORIES")
public class Category extends BaseEntity {

    @Column(name = "NAME")
    private String name;

    @Column(name = "IMAGE")
    private String imagePath;

    @Column(name = "RATING")
    private int rating;

    @JsonBackReference
    @OneToMany(mappedBy = "category", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Product> productList;

}
