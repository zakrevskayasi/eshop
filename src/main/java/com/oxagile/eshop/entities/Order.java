package com.oxagile.eshop.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "ORDERS")
public class Order extends BaseEntity {

    @Column(name = "DATE")
    private String date;

    @Column(name = "PRICE")
    private int price;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @JsonBackReference
    @ManyToMany
    @JoinTable(name = "orders_products", joinColumns = @JoinColumn(name = "ORDER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PRODUCT_ID"))
    private List<Product> productsList;

}
