package com.oxagile.eshop.entities;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "PRODUCT_IMAGES")
public class ProductImage extends BaseEntity {

    @Column(name = "IMAGE")
    private String imagePath;

    @ManyToOne(optional = false)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;

    @Column(name = "PRIMARY_FLAG")
    private int primary;

}
