package com.oxagile.eshop.entitiesDTO;

import lombok.Data;

@Data
public class CategoryDTO {

    private int id;
    private String name;
    private String imagePath;
    private int rating;

    public CategoryDTO(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.imagePath = builder.imagePath;
        this.rating = builder.rating;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private int id;
        private String name;
        private String imagePath;
        private int rating;

        private Builder() {
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withImagePath(String imagePath) {
            this.imagePath = imagePath;
            return this;
        }

        public Builder withRating(int rating) {
            this.rating = rating;
            return this;
        }

        public CategoryDTO build() {
            return new CategoryDTO(this);
        }
    }


}
