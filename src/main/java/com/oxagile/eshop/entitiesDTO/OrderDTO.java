package com.oxagile.eshop.entitiesDTO;

import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entities.User;
import lombok.Data;

import java.util.List;

@Data
public class OrderDTO {

    private int id;
    private String date;
    private int price;
    private User user;
    private List<Product> productsList;

    public OrderDTO(Builder builder) {
        this.id = builder.id;
        this.date = builder.date;
        this.price = builder.price;
        this.user = builder.user;
        this.productsList = builder.productsList;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private int id;
        private String date;
        private int price;
        private User user;
        private List<Product> productsList;

        private Builder() {
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withDate(String date) {
            this.date = date;
            return this;
        }

        public Builder withPrice(int price) {
            this.price = price;
            return this;
        }

        public Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public Builder withListOfProducts(List<Product> productList) {
            this.productsList = productList;
            return this;
        }

        public OrderDTO build() {
            return new OrderDTO(this);
        }
    }

}
