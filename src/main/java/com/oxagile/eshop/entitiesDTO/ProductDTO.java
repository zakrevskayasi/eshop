package com.oxagile.eshop.entitiesDTO;

import lombok.Data;

@Data
public class ProductDTO {

    private int id;
    private String name;
    private String description;
    private int price;

    public ProductDTO(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.price = builder.price;
        this.description = builder.description;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private int id;
        private String name;
        private String description;
        private int price;

        private Builder() {

        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withPrice(int price) {
            this.price = price;
            return this;
        }

        public ProductDTO build() {
            return new ProductDTO(this);
        }


    }

}
