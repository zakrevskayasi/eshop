package com.oxagile.eshop.entitiesDTO;

import lombok.Data;

@Data
public class UserDTO {

    private int id;
    private String name;
    private String surname;
    private String email;
    private String password;
    private String dateOfBirthday;
    private int balance;

    public UserDTO(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.surname = builder.surname;
        this.email = builder.email;
        this.password = builder.password;
        this.dateOfBirthday = builder.dateOfBirthday;
        this.balance = builder.balance;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private int id;
        private String name;
        private String surname;
        private String email;
        private String password;
        private String dateOfBirthday;
        private int balance;

        private Builder() {
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withDateOfBrth(String dateOfBirthday) {
            this.dateOfBirthday = dateOfBirthday;
            return this;
        }

        public Builder withBalance(int balance) {
            this.balance = balance;
            return this;
        }

        public UserDTO build() {
            return new UserDTO(this);
        }

    }

}
