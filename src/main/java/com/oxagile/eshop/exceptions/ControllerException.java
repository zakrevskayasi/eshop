package com.oxagile.eshop.exceptions;

public class ControllerException extends Exception {

    public ControllerException(String message) {
        super(message);
    }

}
