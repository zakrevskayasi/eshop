package com.oxagile.eshop.repositories;

import com.oxagile.eshop.entities.Order;
import com.oxagile.eshop.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    List<Order> findAllByUser(User user);

    List<Order> findAllByUser_Id(int userId);

    @Transactional
    void deleteAllByUserId(int userId);

}
