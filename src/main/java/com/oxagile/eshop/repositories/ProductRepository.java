package com.oxagile.eshop.repositories;

import com.oxagile.eshop.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer>, SearchProductsRepository {

    Page<Product> getAllByCategoryId(int categoryId, Pageable pageable);

    Long countAllByCategoryId(int categoryId);

    List<Product> getAllByCategoryId(int id);

    @Transactional
    void deleteAllByCategoryId(int id);

}
