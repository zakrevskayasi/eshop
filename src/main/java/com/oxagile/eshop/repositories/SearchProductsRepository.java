package com.oxagile.eshop.repositories;

import com.oxagile.eshop.entities.Product;

import java.util.List;
import java.util.Map;

public interface SearchProductsRepository {

    List<Product> findProductsListByParams(Map<String, String> searchParams, int currentPage, int recordsPerPage);

    int getFoundProductsCount(Map<String, String> searchParams, int currentPage, int recordsPerPage);
}
