package com.oxagile.eshop.repositories;

import com.oxagile.eshop.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User getUserByEmail(String email);

    User getUserByEmailAndPassword(String email, String password);

}
