package com.oxagile.eshop.repositories.impl;

import com.oxagile.eshop.entities.Category;
import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entities.Product_;
import com.oxagile.eshop.repositories.SearchProductsRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Repository
public class SearchProductsRepositoryImpl implements SearchProductsRepository {

    @PersistenceContext
    public EntityManager entityManager;

    @Override
    public List<Product> findProductsListByParams(Map<String, String> searchParams, int currentPage, int recordsPerPage) {
        List<Product> products;
        int start = currentPage * recordsPerPage - recordsPerPage;

        List<Predicate> predicates = new ArrayList<>();

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteria = builder.createQuery(Product.class);
        Root<Product> root = criteria.from(Product.class);

        predicates.add(builder.like(root.get("name"), "%" + searchParams.get(SEARCH_KEY) + "%"));

        if (searchParams.containsKey(CATEGORY_SEARCH_KEY)) {
            Join<Product, Category> categoryJoin = root.join(Product_.category);
            predicates.add(builder.equal(categoryJoin.get("name"), searchParams.get(CATEGORY_SEARCH_KEY)));
        }

        if (searchParams.containsKey(PRICE_FROM_KEY)) {
            predicates.add(builder.greaterThanOrEqualTo(root.get("price"), searchParams.get(PRICE_FROM_KEY)));
        }

        if (searchParams.containsKey(PRICE_TO_KEY)) {
            predicates.add(builder.lessThanOrEqualTo(root.get("price"), searchParams.get(PRICE_TO_KEY)));
        }

        criteria.select(root).where(predicates.toArray(new Predicate[]{}));

        if (recordsPerPage > 1) {
            TypedQuery<Product> typedQuery = entityManager.createQuery(criteria);
            typedQuery.setFirstResult(start);
            typedQuery.setMaxResults(recordsPerPage);
            products = typedQuery.getResultList();
        } else {
            products = entityManager.createQuery(criteria).getResultList();
        }

        return products;
    }

    @Override
    public int getFoundProductsCount(Map<String, String> searchParams, int currentPage, int recordsPerPage) {
        return findProductsListByParams(searchParams, currentPage, recordsPerPage).size();
    }


}
