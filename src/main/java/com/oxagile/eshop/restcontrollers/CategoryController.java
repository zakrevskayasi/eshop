package com.oxagile.eshop.restcontrollers;

import com.oxagile.eshop.entities.Category;
import com.oxagile.eshop.entitiesDTO.CategoryDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<CategoryDTO> getAllCategories() throws ServiceException {
        return categoryService.findAllCategories();
    }

    @GetMapping(value = "/{id}")
    public CategoryDTO getCategoryById(@PathVariable int id) throws ServiceException {
        return categoryService.getCategoryById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createCategory(@RequestBody Category category) throws ServiceException {
        categoryService.create(category);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateCategory(@RequestBody Category category) throws ServiceException {
        categoryService.update(category);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteCategory(@PathVariable int id) throws ServiceException {
        categoryService.delete(id);
    }

}
