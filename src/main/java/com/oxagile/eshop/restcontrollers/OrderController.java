package com.oxagile.eshop.restcontrollers;

import com.oxagile.eshop.entities.Order;
import com.oxagile.eshop.entitiesDTO.OrderDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    public List<OrderDTO> getAllOrders() throws ServiceException {
        return orderService.findAll();
    }

    @GetMapping(value = "/{id}")
    public OrderDTO getOrderById(@PathVariable int id) throws ServiceException {
        return orderService.getOrderById(id);
    }

    @GetMapping(value = "/user/{userId}")
    public List<Order> getOrdersByUserId(@PathVariable int userId) throws ServiceException {
        return orderService.getOrdersByUserId(userId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createOrder(@RequestBody Order order) throws ServiceException {
        orderService.create(order);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateOrder(@RequestBody Order order) throws ServiceException {
        orderService.update(order);
    }

    @DeleteMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOrder(@PathVariable int id) throws ServiceException {
        orderService.delete(id);
    }

    @DeleteMapping(value = "/user/{userId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAllOrdersForUser(@PathVariable int userId) throws ServiceException {
        orderService.deleteAllOrdersForUser(userId);
    }
}
