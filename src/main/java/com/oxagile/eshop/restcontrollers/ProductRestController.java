package com.oxagile.eshop.restcontrollers;

import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entitiesDTO.ProductDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductRestController {

    private final ProductService productService;

    @Autowired
    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/products")
    public List<ProductDTO> getAllProducts() throws ServiceException {
        return productService.findAll();
    }

    @GetMapping(value = "/{category_id}/products")
    public List<Product> getProductsForCategory(@PathVariable int category_id) throws ServiceException {
        return productService.getProductsForCategory(category_id);
    }

    @GetMapping(value = "/products/{id}")
    public ProductDTO getProductById(@PathVariable int id) throws ServiceException {
        return productService.getProductById(id);
    }

    @PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createProduct(@RequestBody Product product) throws ServiceException {
        productService.create(product);
    }

    @PutMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateProduct(@RequestBody Product product) throws ServiceException {
        productService.update(product);
    }

    @DeleteMapping(value = "/product/{id}")
    public void deleteProduct(@PathVariable int id) throws ServiceException {
        productService.delete(id);
    }

    @DeleteMapping(value = "/products/{categoryId}")
    public void deleteAllProductsInCategory(@PathVariable int categoryId) throws ServiceException {
        productService.deleteAllByCategoryId(categoryId);
    }


}
