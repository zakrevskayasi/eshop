package com.oxagile.eshop.restcontrollers;

import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.oxagile.eshop.constants.CategoryConstants.SELECT_CATEGORY_DROP_DOWN_MSG;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@RestController
@RequestMapping("/api")
public class SearchRestController {

    private final ProductService productService;

    @Autowired
    public SearchRestController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/search")
    public List<Product> search(@RequestParam(SEARCH_KEY) String searchKey,
                                @RequestParam(value = CATEGORY_SEARCH_KEY, required = false) String category,
                                @RequestParam(value = PRICE_FROM_KEY, required = false) String priceFrom,
                                @RequestParam(value = PRICE_TO_KEY, required = false) String priceTo) throws ServiceException {

        Map<String, String> searchParams = new HashMap<>();

        if (searchKey != null && !searchKey.isEmpty()) {
            searchParams.put(SEARCH_KEY, searchKey);
        }

        if (category != null && !category.isEmpty() && !category.equals(SELECT_CATEGORY_DROP_DOWN_MSG)) {
            searchParams.put(CATEGORY_SEARCH_KEY, category);
        }

        if (priceFrom != null && !priceFrom.isEmpty()) {
            searchParams.put(PRICE_FROM_KEY, priceFrom);
        }

        if (priceTo != null && !priceTo.isEmpty()) {
            searchParams.put(PRICE_TO_KEY, priceTo);
        }

        return productService.findProductsListByParams(searchParams, 0, 0);
    }
}
