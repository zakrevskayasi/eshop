package com.oxagile.eshop.restcontrollers;

import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.entitiesDTO.UserDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<UserDTO> getAllUsers() throws ServiceException {
        return userService.findAll();
    }

    @GetMapping(value = "/{id}")
    public UserDTO getUsersById(@PathVariable int id) throws ServiceException {
        return userService.findUserById(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createUser(@RequestBody User user) throws ServiceException {
        userService.create(user);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateUser(@RequestBody User user) throws ServiceException {
        userService.update(user);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUser(@PathVariable int id) throws ServiceException {
        userService.delete(id);
    }

}
