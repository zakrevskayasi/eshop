package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.BaseEntity;
import com.oxagile.eshop.exceptions.ServiceException;

import java.util.Optional;

public interface BaseService<T extends BaseEntity> {

    T create(T entity) throws ServiceException;

    Optional<T> read(int id) throws ServiceException;

    void update(T entity) throws ServiceException;

    void delete(int id) throws ServiceException;
}