package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.Category;
import com.oxagile.eshop.entitiesDTO.CategoryDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

public interface CategoryService extends BaseService<Category> {

    List<Category> getAllCategories() throws ServiceException;

    Category getCategoryWithLimitedProductList(int id, int currentPage, int recordsPerPage) throws ServiceException;

    ModelAndView getCategoryModel(String id, String recordsPerPage, String currentPageNum) throws ServiceException;

    List<CategoryDTO> findAllCategories()throws ServiceException;

    CategoryDTO getCategoryById(int id) throws ServiceException;

}
