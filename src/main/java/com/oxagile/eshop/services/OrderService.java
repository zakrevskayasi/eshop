package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.Order;
import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.entitiesDTO.OrderDTO;
import com.oxagile.eshop.exceptions.ServiceException;

import java.util.List;

public interface OrderService extends BaseService<Order> {

    List<Order> getOrdersListForUser(User user) throws ServiceException;

    List<OrderDTO> findAll() throws ServiceException;

    List<Order> getOrdersByUserId(int userId) throws ServiceException;

    OrderDTO getOrderById(int id) throws ServiceException;

    void deleteAllOrdersForUser(int userId) throws ServiceException;
}
