package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.ProductImage;

public interface ProductImageService extends BaseService<ProductImage> {
}
