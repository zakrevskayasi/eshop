package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entitiesDTO.ProductDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface ProductService extends BaseService<Product> {

    Long getCountOfProductsForCategory(int categoryId) throws ServiceException;

    List<Product> findProductsListByParams(Map<String, String> params, int currentPage, int recordsPerPage) throws ServiceException;

    Page<Product> findProductsByCategoryId(int categoryId, Pageable pageable) throws ServiceException;

    List<Product> getProductsForCategory(int categoryId) throws ServiceException;

    List<ProductDTO> findAll() throws ServiceException;

    ProductDTO getProductById(int id) throws ServiceException;

    void deleteAllByCategoryId(int id) throws ServiceException;

    int getFoundProductsCount(Map<String, String> params, int currentPage, int recordsPerPage);

}
