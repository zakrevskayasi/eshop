package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.Category;
import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.exceptions.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.oxagile.eshop.constants.CategoryConstants.SELECT_CATEGORY_DROP_DOWN_MSG;
import static com.oxagile.eshop.constants.PagesPathesPool.SEARCH_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Service("searchService")
public class SearchService {

    private final CategoryService categoryService;

    private final ProductService productService;

    @Autowired
    public SearchService(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    public ModelAndView search(HttpServletRequest request, String searchKey, String category, String priceFrom, String priceTo, String recordsPerPage, String productsListCurrentPage) throws ServiceException {
        ModelMap model;

        List<Category> categoriesList = categoryService.getAllCategories();

        Map<String, String> searchParams = fillSearchParams(request, searchKey, category, priceFrom, priceTo);

        int numberOfPages = 1;
        int currentPage = 1;

        int productsCount = productService.getFoundProductsCount(searchParams, 0, 0);

        if (recordsPerPage == null) {
            recordsPerPage = String.valueOf(DEFAULT_RECORDS_COUNT_PER_PAGE);
        }

        if (recordsPerPage.equals("all")) {
            model = fillModel(searchParams, numberOfPages, currentPage, productsCount);
        } else {
            if (!(productsListCurrentPage == null)) {
                currentPage = Integer.parseInt(productsListCurrentPage);
            }

            int recordsCount = Integer.parseInt(recordsPerPage);

            numberOfPages = productsCount / recordsCount;

            if (numberOfPages % recordsCount > 0) {
                numberOfPages++;
            }

            if (recordsCount >= productsCount || currentPage > numberOfPages) {
                currentPage = 1;
                numberOfPages = 1;
            }

            model = fillModel(searchParams, numberOfPages, currentPage, recordsCount);
            model.addAttribute(PRODUCT_RECORDS_PER_PAGE, recordsPerPage);
        }

        model.addAttribute(ALL_CATEGORIES_LIST_PARAM, categoriesList);

        return new ModelAndView(SEARCH_PAGE, model);

    }

    private Map<String, String> fillSearchParams(HttpServletRequest request, String searchKey, String categorySearchKey, String priceFromKey, String priceToKey) {
        Map<String, String> searchParams = new HashMap<>();

        if (searchKey != null && !searchKey.isEmpty()) {
            searchParams.put(SEARCH_KEY, searchKey);
        }

        if (categorySearchKey != null && !categorySearchKey.isEmpty() && !categorySearchKey.equals(SELECT_CATEGORY_DROP_DOWN_MSG)) {
            searchParams.put(CATEGORY_SEARCH_KEY, categorySearchKey);
            request.getSession().setAttribute(CATEGORY_SEARCH_KEY, categorySearchKey);
        } else {
            request.getSession().setAttribute(CATEGORY_SEARCH_KEY, "");
        }

        if (priceFromKey != null && !priceFromKey.isEmpty()) {
            searchParams.put(PRICE_FROM_KEY, priceFromKey);
            request.getSession().setAttribute(PRICE_FROM_KEY, priceFromKey);
        } else {
            request.getSession().setAttribute(PRICE_FROM_KEY, "");
        }

        if (priceToKey != null && !priceToKey.isEmpty()) {
            searchParams.put(PRICE_TO_KEY, priceToKey);
            request.getSession().setAttribute(PRICE_TO_KEY, priceToKey);
        } else {
            request.getSession().setAttribute(PRICE_TO_KEY, "");
        }

        return searchParams;
    }

    private ModelMap fillModel(Map<String, String> searchParams, int numberOfPages, int currentPage, int recordsCount) throws ServiceException {
        ModelMap modelMap = new ModelMap();
        List<Product> productList = productService.findProductsListByParams(searchParams, currentPage, recordsCount);
        modelMap.addAttribute(FOUNDED_PRODUCTS_LIST, productList);
        modelMap.addAttribute(SEARCH_KEY, searchParams.get(SEARCH_KEY));
        modelMap.addAttribute(PRODUCTS_LIST_CURRENT_PAGE, currentPage);
        modelMap.addAttribute(NUMBER_OF_PRODUCT_PAGES, numberOfPages);

        return modelMap;
    }

}
