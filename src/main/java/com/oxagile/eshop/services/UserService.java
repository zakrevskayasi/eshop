package com.oxagile.eshop.services;

import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.entitiesDTO.UserDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

public interface UserService extends BaseService<User> {

    User getUserByCreds(String email, String password) throws ServiceException;

    User getUserByEmail(String email) throws ServiceException;

    List<UserDTO> findAll() throws ServiceException;

    UserDTO findUserById(int id) throws ServiceException;

    ModelAndView getNewUserModel(String username, String surname, String brthday, String brthmonth, String brthYear, String email, String password, String repeatPass) throws ServiceException;

}
