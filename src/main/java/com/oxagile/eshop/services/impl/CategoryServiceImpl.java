package com.oxagile.eshop.services.impl;

import com.oxagile.eshop.entities.Category;
import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entitiesDTO.CategoryDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.repositories.CategoryRepository;
import com.oxagile.eshop.services.CategoryService;
import com.oxagile.eshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.oxagile.eshop.constants.PagesPathesPool.CATEGORY_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.*;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    private final ProductService productService;

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, ProductService productService) {
        this.categoryRepository = categoryRepository;
        this.productService = productService;
    }

    public Category create(Category category) {
        return categoryRepository.saveAndFlush(category);
    }

    public Optional<Category> read(int id) {
        return categoryRepository.findById(id);
    }

    public void update(Category category) throws ServiceException {
        Category foundCategory = read(category.getId()).orElseThrow(() -> new ServiceException("Category with id" + category.getId() + " wasn't found!"));

        foundCategory.setId(category.getId());
        foundCategory.setName(category.getName());
        foundCategory.setImagePath(category.getImagePath());
        foundCategory.setRating(category.getRating());

        categoryRepository.saveAndFlush(foundCategory);
    }

    public void delete(int id) {
        categoryRepository.deleteById(id);
    }

    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategoryWithLimitedProductList(int id, int currentPage, int recordsPerPage) throws ServiceException {

        Category category = categoryRepository.findById(id).orElseThrow(() -> new ServiceException("Category with id" + id + " wasn't found!"));

        Page<Product> limitedProducts = productService.findProductsByCategoryId(id, PageRequest.of(currentPage - 1, recordsPerPage));
        category.setProductList(limitedProducts.getContent());

        return category;
    }

    @Override
    public ModelAndView getCategoryModel(String id, String recordsPerPage, String currentPageNum) throws ServiceException {
        int numberOfPages = 1;
        int currentPage = 1;
        int categoryId = Integer.parseInt(id);

        ModelAndView modelAndView;

        int productsCount = productService.getCountOfProductsForCategory(categoryId).intValue();

        if (recordsPerPage.equals("all")) {
            modelAndView = fillResponse(numberOfPages, currentPage, categoryId, productsCount, recordsPerPage);
        } else {
            currentPage = Integer.parseInt(currentPageNum);

            int recordsCount = Integer.parseInt(recordsPerPage);

            numberOfPages = productsCount / recordsCount;

            if (numberOfPages % recordsCount > 0) {
                numberOfPages++;
            }

            if (recordsCount >= productsCount || currentPage > numberOfPages) {
                currentPage = 1;
                numberOfPages = 1;
            }

            modelAndView = fillResponse(numberOfPages, currentPage, categoryId, recordsCount, recordsPerPage);
        }

        return modelAndView;
    }

    @Override
    public List<CategoryDTO> findAllCategories() {
        List<Category> foundCategories = categoryRepository.findAll();
        List<CategoryDTO> preparedCategories = new ArrayList<>();

        if (!foundCategories.isEmpty()) {
            for (Category category : foundCategories) {
                preparedCategories.add(CategoryDTO.newBuilder()
                        .withId(category.getId())
                        .withName(category.getName())
                        .withImagePath(category.getImagePath())
                        .withRating(category.getRating())
                        .build());
            }
        }

        return preparedCategories;
    }

    @Override
    public CategoryDTO getCategoryById(int id) throws ServiceException {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new ServiceException("Category with id " + id + " wasn't found!"));
        return CategoryDTO.newBuilder()
                .withId(category.getId())
                .withName(category.getName())
                .withImagePath(category.getImagePath())
                .withRating(category.getRating())
                .build();
    }

    private ModelAndView fillResponse(long numberOfPages, int currentPage, int categoryId, int recordsCount, String recordsPerPage) throws ServiceException {
        ModelMap model = new ModelMap();
        Category category = getCategoryWithLimitedProductList(categoryId, currentPage, recordsCount);

        model.addAttribute(CATEGORY_ID_PARAM, categoryId);
        model.addAttribute(NUMBER_OF_PRODUCT_PAGES, numberOfPages);
        model.addAttribute(PRODUCTS_LIST_CURRENT_PAGE, currentPage);
        model.addAttribute(PRODUCT_RECORDS_PER_PAGE, recordsPerPage);
        model.addAttribute(CATEGORY_PARAM, category);

        return new ModelAndView(CATEGORY_PAGE, model);
    }

}
