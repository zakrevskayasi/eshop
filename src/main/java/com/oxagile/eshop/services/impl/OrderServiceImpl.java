package com.oxagile.eshop.services.impl;

import com.oxagile.eshop.entities.Order;
import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.entitiesDTO.OrderDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.repositories.OrderRepository;
import com.oxagile.eshop.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public Order create(Order order) {
        return orderRepository.saveAndFlush(order);
    }

    public Optional<Order> read(int id) {
        return orderRepository.findById(id);
    }

    public void update(Order order) throws ServiceException {
        Order foundOrder = read(order.getId()).orElseThrow(() -> new ServiceException("Order with id " + order.getId() + " wasn't found!"));

        foundOrder.setUser(order.getUser());
        foundOrder.setProductsList(order.getProductsList());
        foundOrder.setDate(order.getDate());
        foundOrder.setPrice(order.getPrice());

        orderRepository.saveAndFlush(foundOrder);
    }

    public void delete(int id) {
        orderRepository.deleteById(id);
    }

    @Override
    public List<Order> getOrdersListForUser(User user) {
        return orderRepository.findAllByUser(user);
    }

    @Override
    public List<OrderDTO> findAll() {
        List<Order> foundOrders = orderRepository.findAll();
        List<OrderDTO> preparedOrders = new ArrayList<>();

        if (!foundOrders.isEmpty()) {
            for (Order order : foundOrders) {

                preparedOrders.add(OrderDTO.newBuilder()
                        .withId(order.getId())
                        .withDate(order.getDate())
                        .withPrice(order.getPrice())
                        .withListOfProducts(order.getProductsList())
                        .withUser(order.getUser())
                        .build());
            }
        }

        return preparedOrders;
    }

    @Override
    public List<Order> getOrdersByUserId(int userId) {
        return orderRepository.findAllByUser_Id(userId);
    }

    @Override
    public OrderDTO getOrderById(int id) throws ServiceException {
        Order order = orderRepository.findById(id).orElseThrow(() -> new ServiceException("Order with id " + id + " wasn't found!"));

        return OrderDTO.newBuilder()
                .withId(order.getId())
                .withDate(order.getDate())
                .withPrice(order.getPrice())
                .withListOfProducts(order.getProductsList())
                .withUser(order.getUser())
                .build();
    }

    @Override
    public void deleteAllOrdersForUser(int userId) {
        orderRepository.deleteAllByUserId(userId);
    }

}
