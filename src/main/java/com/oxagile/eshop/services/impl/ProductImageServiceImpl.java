package com.oxagile.eshop.services.impl;

import com.oxagile.eshop.entities.ProductImage;
import com.oxagile.eshop.repositories.ProductImageRepository;
import com.oxagile.eshop.services.ProductImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("productImageService")
public class ProductImageServiceImpl implements ProductImageService {

    private final ProductImageRepository productImageRepository;

    @Autowired
    public ProductImageServiceImpl(ProductImageRepository productImageRepository) {
        this.productImageRepository = productImageRepository;
    }

    public ProductImage create(ProductImage image) {
        return productImageRepository.saveAndFlush(image);
    }

    public Optional<ProductImage> read(int id) {
        return productImageRepository.findById(id);
    }

    public void update(ProductImage image) {
        productImageRepository.saveAndFlush(image);
    }

    public void delete(int id) {
        productImageRepository.deleteById(id);
    }
}
