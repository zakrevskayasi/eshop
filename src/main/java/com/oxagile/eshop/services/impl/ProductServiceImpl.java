package com.oxagile.eshop.services.impl;

import com.oxagile.eshop.entities.Product;
import com.oxagile.eshop.entitiesDTO.ProductDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.repositories.ProductRepository;
import com.oxagile.eshop.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service("productService")
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product create(Product product) {
        return productRepository.saveAndFlush(product);
    }

    public Optional<Product> read(int id) {
        return productRepository.findById(id);
    }

    public void update(Product product) throws ServiceException {
        Optional<Product> foundProduct = read(product.getId());
        foundProduct.orElseThrow(() -> new ServiceException("Product with id " + product.getId() + " wasn't found!"));

        foundProduct.get().setName(product.getName());
        foundProduct.get().setDescription(product.getDescription());
        foundProduct.get().setImages(product.getImages());
        foundProduct.get().setCategory(product.getCategory());
        foundProduct.get().setPrice(product.getPrice());
        foundProduct.get().setOrders(product.getOrders());

        productRepository.save(foundProduct.get());
    }

    public void delete(int id) {
        productRepository.deleteById(id);
    }

    public Long getCountOfProductsForCategory(int categoryId) {
        return productRepository.countAllByCategoryId(categoryId);
    }

    @Override
    public List<Product> findProductsListByParams(Map<String, String> params, int currentPage, int recordsPerPage) {
        return productRepository.findProductsListByParams(params, currentPage, recordsPerPage);
    }

    @Override
    public Page<Product> findProductsByCategoryId(int categoryId, Pageable pageable) {
        return productRepository.getAllByCategoryId(categoryId, pageable);
    }

    @Override
    public List<Product> getProductsForCategory(int categoryId) {
        return productRepository.getAllByCategoryId(categoryId);
    }

    @Override
    public List<ProductDTO> findAll() {
        List<Product> foundProducts = productRepository.findAll();
        List<ProductDTO> preparedProducts = new ArrayList<>();

        if (!foundProducts.isEmpty()) {
            for (Product product : foundProducts) {
                preparedProducts.add(ProductDTO.newBuilder()
                        .withId(product.getId())
                        .withName(product.getName())
                        .withPrice(product.getPrice())
                        .withDescription(product.getDescription())
                        .build());
            }
        }

        return preparedProducts;
    }

    @Override
    public ProductDTO getProductById(int id) throws ServiceException {
        Product product = productRepository.findById(id).orElseThrow(() -> new ServiceException("Product with id " + id + " wasn't found!"));

        return ProductDTO.newBuilder()
                .withId(product.getId())
                .withName(product.getName())
                .withPrice(product.getPrice())
                .withDescription(product.getDescription())
                .build();
    }

    @Override
    public void deleteAllByCategoryId(int id) {
        productRepository.deleteAllByCategoryId(id);
    }

    @Override
    public int getFoundProductsCount(Map<String, String> params, int currentPage, int recordsPerPage) {
        return productRepository.getFoundProductsCount(params, currentPage, recordsPerPage);
    }

}
