package com.oxagile.eshop.services.impl;

import com.oxagile.eshop.entities.User;
import com.oxagile.eshop.entitiesDTO.UserDTO;
import com.oxagile.eshop.exceptions.ServiceException;
import com.oxagile.eshop.repositories.UserRepository;
import com.oxagile.eshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.oxagile.eshop.constants.PagesPathesPool.CABINET_PAGE;
import static com.oxagile.eshop.constants.PagesPathesPool.SIGN_UP_PAGE;
import static com.oxagile.eshop.constants.RequestParamsConstants.DEFAULT_BALANCE;
import static com.oxagile.eshop.constants.RequestParamsConstants.USER_DATA;
import static com.oxagile.eshop.utils.ValidateUtils.checkDateOfBirthday;
import static com.oxagile.eshop.utils.ValidateUtils.validateEmail;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(User user) {
        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        return userRepository.saveAndFlush(user);
    }

    public Optional<User> read(int id) {
        return userRepository.findById(id);
    }

    public void update(User user) throws ServiceException {
        user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
        Optional<User> usr = read(user.getId());
        usr.orElseThrow(() -> new ServiceException("User with id " + user.getId() + " not found!"));

        usr.get().setName(user.getName());
        usr.get().setSurname(user.getSurname());
        usr.get().setPassword(user.getPassword());
        usr.get().setEmail(user.getEmail());
        usr.get().setBalance(user.getBalance());
        usr.get().setDateOfBirthday(user.getDateOfBirthday());

        userRepository.save(usr.get());
    }

    public void delete(int id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getUserByCreds(String email, String password) {
        return userRepository.getUserByEmailAndPassword(email, password);
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public List<UserDTO> findAll() {
        List<User> foundUsers = userRepository.findAll();
        List<UserDTO> preparedUsers = new ArrayList<>();

        if (!foundUsers.isEmpty()) {
            for (User user : foundUsers) {
                preparedUsers.add(UserDTO.newBuilder()
                        .withId(user.getId())
                        .withName(user.getName())
                        .withSurname(user.getSurname())
                        .withDateOfBrth(user.getDateOfBirthday())
                        .withEmail(user.getEmail())
                        .withPassword(user.getPassword())
                        .withBalance(user.getBalance())
                        .build());
            }
        }
        return preparedUsers;
    }

    @Override
    public UserDTO findUserById(int id) throws ServiceException {
        User user = userRepository.findById(id).orElseThrow(() -> new ServiceException("User with id " + id + " not found!"));

        return UserDTO.newBuilder()
                .withId(user.getId())
                .withName(user.getName())
                .withSurname(user.getSurname())
                .withDateOfBrth(user.getDateOfBirthday())
                .withEmail(user.getEmail())
                .withPassword(user.getPassword())
                .withBalance(user.getBalance())
                .build();
    }

    @Override
    public ModelAndView getNewUserModel(String username, String surname, String brthday, String brthmonth, String brthYear, String email, String password, String repeatPass) {
        ModelMap modelMap = new ModelMap();
        ModelAndView modelAndView;
        String dateOfBrth = brthday + "-" + brthmonth + "-" + brthYear;

        if (!username.isEmpty()
                && !surname.isEmpty()
                && !email.isEmpty()
                && !password.isEmpty()
                && !repeatPass.isEmpty()
                && validateEmail(email)
                && !dateOfBrth.isEmpty()
                && checkDateOfBirthday(brthYear)
                && password.equals(repeatPass)) {

            User user = User.builder().build();
            user.setName(username);
            user.setSurname(surname);
            user.setEmail(email);
            user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
            user.setDateOfBirthday(dateOfBrth);
            user.setBalance(DEFAULT_BALANCE);

            create(user);
            modelMap.addAttribute(USER_DATA, user);
            modelAndView = new ModelAndView(CABINET_PAGE, modelMap);
        } else {
            modelAndView = new ModelAndView(SIGN_UP_PAGE);
        }

        return modelAndView;
    }

}
