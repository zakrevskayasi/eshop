package com.oxagile.eshop.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateUtils {
    private DateUtils() {
    }

    public static String formatLocalDateToString(LocalDate dateToParse, String format) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format, Locale.ENGLISH);
        return dateToParse.format(dtf);
    }
}
