<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Личный кабинет</title>
        <link rel="stylesheet" type="text/css" href="resources/mystyle.css">
    </head>
    <body>
        <c:import url="/WEB-INF/views/header.jsp" />
        <c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
        <form method="post" action="${basePath}/home">
            <button id="mainPageBtn" type="submit">Главная</button>
        </form>
        <form id="userInfoBlock"></br>
            <label id="privateInfo">Личные данные:</label></br>
            <input id="userName" value="${user.getName()}" disabled=true>
            <input id="userSurname" value="${user.getSurname()}" disabled=true>
            <input id="userBrth" value="${user.getDateOfBirthday()}" disabled=true>
            <input id="userEmail" value="${user.getEmail()}" disabled=true>
        </form>
        <div id="orderHistory"></br>
            <label id="orderHistoryLbl">История заказов:</label>
            <c:forEach items="${ordersList}" var="order">
                <div id="ordersList"> </br>
                    <span id="orderData">Заказ №: ${order.getId()} / ${order.getDate()}</span> </br>
                    <c:forEach items="${order.getProductsList()}" var="product">
                        <div id="container">
                            <div id="product-info">
                                <a id="productRedirectLink" href="${basePath}/product?product_id=${product.getId()}">
                                    <c:forEach items="${product.getImages()}" var="image">
                                        <c:if test="${image.getPrimary() eq 2}">
                                            <img class="img1" src="${basePath}${image.getImagePath()}" style="padding: 5px;">
                                        </c:if>
                                    </c:forEach>
                                </a>
                            </div>
                            <div id="product_name">
                                <span>${product.getName()}</span>
                            </div>
                            <div id="product_description">
                                <span>${product.getDescription()}</span>
                            </div>
                            <div id="product_price">
                                <span>${product.getPrice()}</span>
                            </div>
                            <div id="empty"></div>
                        </div>
                    </c:forEach>
                </div>
                <div></div>
                </br>
            </c:forEach>
        </div>
    </body>
</html>
