<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <title>Корзина</title>
        <link rel="stylesheet" type="text/css" href="resources/mystyle.css">
    </head>
    <body>
        <c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
        <div style="display: inline-block">
            <c:import url="/WEB-INF/views/header.jsp"/>
            <form method="post" action="${basePath}/home">
                <button id="mainPageBtn" type="submit">Главная</button>
            </form>
        </div>

        <div id="productsListContainer">
            <c:choose>
                <c:when test="${!empty sessionScope.cart.getProducts()}">
                    <c:forEach items="${sessionScope.cart.getProducts()}" var="product">
                        <div id="prodData">
                            <div id="prodImg">
                                <a id="productRedirectLink" href="${basePath}/product?product_id=${product.getId()}">
                                    <c:forEach items="${product.getImages()}" var="image">
                                        <c:if test="${image.getPrimary() eq 2}">
                                            <img class="img1" src="${basePath}${image.getImagePath()}" style="margin-top: 34px">
                                        </c:if>
                                    </c:forEach>
                                </a>
                            </div>
                            <div id="prodName">
                                <span>Название: ${product.getName()}</span>
                            </div>
                            <div id="prodDesc">
                                <span>${product.getDescription()}</span>
                            </div>
                            <div id="prodPrice">
                                <span>Цена: ${product.getPrice()}$</span>
                            </div>
                            <a href="${basePath}/deleteProductFromCart?product_id=${product.getId()}">
                                <button id="deleteProductBtn" type="submit">Удалить</button>
                            </a>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div id="emptyCartMsg">
                        <span>В корзине нет товаров</span>
                    </div>
                </c:otherwise>
            </c:choose>
        </div> </br>
        <form method="post" action="${basePath}/createOrder">
            <button id="createOrder" type="submit">Оформить заказ</button>
        </form>
    </body>
</html>
