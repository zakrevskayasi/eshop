<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Категория</title>
        <link rel="stylesheet" type="text/css" href="resources/mystyle.css">
    </head>
    <body>
        <c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
        <div style="display: inline-block">
            <c:import url="/WEB-INF/views/header.jsp"/>
            <form method="post" action="${basePath}/home">
                <button id="mainPageBtn" type="submit">Главная</button>
            </form>
        </div>
        <div id="recordsDisplayCount">
            <span id="catgoryName">${category.getName()}</span>
            <span id="recordsCount">Показывать по:</span>
            <a id="pagesize_10" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=10&currentPage=1">10</a>
            <a id="pagesize_20 active" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=20&currentPage=1">20</a>
            <a id="pagesize_50" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=50&currentPage=1">50</a>
            <a id="pagesize_all" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=all&currentPage=1">Все</a>
        </div> </br>

        <div id="productsListContainer">
            <c:forEach items="${category.getProductList()}" var="product">
                <div id="prodData">
                    <div id="prodImg">
                        <a id="productRedirectLink" href="${basePath}/product?product_id=${product.getId()}">
                            <c:forEach items="${product.getImages()}" var="image">
                                <c:if test="${image.getPrimary() eq 2}">
                                    <img class="img1" src="${basePath}${image.getImagePath()}" style="margin-top: 34px">
                                </c:if>
                            </c:forEach>
                        </a>
                    </div>
                    <div id="prodName">
                        <span>Название: ${product.getName()}</span>
                    </div>
                    <div id="prodDesc">
                        <span>${product.getDescription()}</span>
                    </div>
                    <div id="prodPrice">
                        <span>Цена: ${product.getPrice()}$</span>
                    </div>
                </div>
            </c:forEach>
            <div id="categoryListPaginationContainer">
                <ul id="pagesList">
                    <c:if test="${currentPage != 1 and currentPage > 0}">
                        <li class="page-item">
                            <a class="page-link" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}">Previous</a>
                        </li>
                    </c:if>

                    <c:forEach begin="1" end="${numberOfPages}" var="i">
                        <c:choose>
                            <c:when test="${currentPage eq i}">
                                <li class="page-item active">
                                    <a class="page-link">
                                        ${i}
                                    </a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item">
                                    <a class="page-link" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=${recordsPerPage}&currentPage=${i}">${i}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>

                    <c:if test="${currentPage lt numberOfPages}">
                        <li class="page-item">
                            <a class="page-link" href="${basePath}/category?category_id=${category.getId()}&recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}">Next</a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </body>
</html>
