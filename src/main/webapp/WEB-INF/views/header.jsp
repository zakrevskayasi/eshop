<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div>
    <c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
    <form method="post">
        <button id="cmsButton" type="submit" name="command" value="redirect-to-cms">Перейти в CMS</button>
    </form>
    <form method="post" action="${basePath}/cabinet">
        <button id="cabinet" type="submit">Кабинет</button>
    </form>
    <form method="post" action="${basePath}/openCart">
        <button id="cart" type="submit">Корзина</button>
    </form>
</div>
