<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
	<head>
		<title>Главная</title>
		<link rel="stylesheet" type="text/css" href="resources/mystyle.css">
	</head>
	<body>
        <c:import url="/WEB-INF/views/header.jsp" />
		<c:set var = "baseImagePath" value = "${pageContext.request.contextPath}"/>
		<fmt:setBundle basename="application" var="application"/>

		<form action="${baseImagePath}/search">
			<div>
				<input type="search" name="searchKey">
				<button id="search" type="submit">Поиск
				</button>
			</div>
		</form>

		<div>
			<label type="categoriesList">Каталог</label>
		</div>
        <c:if test="${not empty categoriesList}">
            <div type="categoriesLinksList">
                <c:forEach items="${categoriesList}" var="category">
                    <div type="category">
						<a href="${baseImagePath}/category?category_id=${category.getId()}&recordsPerPage=${recordsPerPage}&currentPage=1">${category.getName()}</a>
						<img src="${baseImagePath}${category.getImagePath()}">
                    </div>
                </c:forEach>
            </div>
        </c:if>
	</body>
</html>