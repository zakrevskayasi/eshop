<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
	<head>
		<title>Вход в личный кабинет</title>
		<link rel="stylesheet" type="text/css" href="resources/mystyle.css">
	</head>
	<body>
		<c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
		<div id="container">

			<form id="loginForm" method="post" action="${basePath}/login">
				<div id="cabinetLbl">
					<label>Личный кабинет</label>
				</div>
				<input id="usrn" type="text" placeholder="Логин" name="username">
				<input id="usrpsw" type="password" placeholder="Пароль" name="password">
				<div id="lower">
					<button id="loginBtn" type="submit">Вход</button>
				</div>
			</form>
			<form id="signupForm" method="post" action="${basePath}/openSignUpPage">
				<button id="signupBtn" type="submit">Регистрация</button>
			</form>
		</div>
	</body>
</html>
