<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Продукт</title>
        <link rel="stylesheet" type="text/css" href="resources/mystyle.css">
        <script>
            function productAddedToShoppingCartMsg() {
                window.confirm("Продукт добавлен в корзину!");
            }
        </script>
    </head>
    <body>
        <c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
        <div style="display: inline-block">
            <c:import url="/WEB-INF/views/header.jsp"/>
            <form method="post" action="${basePath}/home">
                <button id="mainPageBtn" type="submit">Главная</button>
            </form>
        </div>
        <div id="productContainer">
            <div id="pImage">
                <c:forEach items="${product.getImages()}" var="image">
                    <c:if test="${image.getPrimary() eq 1}">
                        <img class="img1" src="${basePath}${image.getImagePath()}" style="padding: 30px;">
                    </c:if>
                </c:forEach>
            </div>
            <div id="pName">
                <span>${product.getName()}</span>
            </div>
            <div id="pDescription">
                <span>${product.getDescription()}</span>
            </div>
            <div id="pPrice">
                <span>${product.getPrice()}$</span>
            </div>

            <a href="${basePath}/addProductToCart?product_id=${product.getId()}">
                <button id="buyBtn" type="submit" onclick="productAddedToShoppingCartMsg()">Купить</button>
            </a>

        </div> </br>
        <div id="imgContainer">
            <c:forEach items="${product.getImages()}" var="image">
                <c:if test="${image.getPrimary() eq 0}">
                    <div id="backgroundImg">
                        <a href="${basePath}${image.getImagePath()}" target="new">
                            <img class="additionalImg" src="${basePath}${image.getImagePath()}">
                        </a>
                    </div>
                </c:if>
            </c:forEach>
        </div>
    </body>
</html>
