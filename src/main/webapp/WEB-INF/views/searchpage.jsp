<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Поиск</title>
        <link rel="stylesheet" type="text/css" href="resources/mystyle.css">
    </head>
    <body>
        <c:set var = "basePath" value = "${pageContext.request.contextPath}"/>
        <div style="display: inline-block">
            <c:import url="/WEB-INF/views/header.jsp"/>
            <form method="post" action="${basePath}/home">
                <button id="mainPageBtn" type="submit">Главная</button>
            </form>
        </div>
        <form action="${basePath}/search">
            <div>
                <input type="search" name="searchKey" value="${searchKey}">
                <button id="search" type="submit">Поиск</button>
            </div>
            <div id="filter">
                <label id="filterLbl">
                    Фильтр
                </label> </br>
                <select id="searchCategoryList" name="categorySearchKey">
                    <option value="" style="font-size: 20px">Выберите категорию...</option>
                    <c:forEach items="${allCategoriesList}" var="category">
                        <option value="${category.getName()}" ${category.getName() == categorySearchKey ? 'selected="selected"' : ''} style="font-size: 20px">${category.getName()}</option>
                    </c:forEach>
                </select> </br>
                <input id="priceFrom" name="priceFromKey" value="${priceFromKey}" placeholder ="Цена от">
                <input id="priceTo" name="priceToKey" value="${priceToKey}" placeholder="Цена до"> </br>
                <button id="applyFilterBtn">Применить</button>
            </div>
        </form>
        <div id="foundedProductsList">
            <div id="searchResultsCount">
                <span id="searchRecordsCount">Показывать по:</span>
                <a id="pagesize_10" href="${basePath}/search?recordsPerPage=10&currentPage=1&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">10</a>
                <a id="pagesize_20" href="${basePath}/search?recordsPerPage=20&currentPage=1&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">20</a>
                <a id="pagesize_50" href="${basePath}/search?recordsPerPage=50&currentPage=1&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">50</a>
                <a id="pagesize_all" href="${basePath}/search?recordsPerPage=all&currentPage=1&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">Все</a>
            </div> </br>
            <c:choose>
                <c:when test="${searchResultList.size() > 0}">
                    <c:forEach items="${searchResultList}" var="product">
                        <div id="container">
                            <div id="product-info">
                                <a id="productRedirectLink" href="${basePath}/product?product_id=${product.getId()}">
                                    <c:forEach items="${product.getImages()}" var="image">
                                        <c:if test="${image.getPrimary() eq 2}">
                                            <img class="img1" src="${basePath}${image.getImagePath()}" style="padding: 5px;">
                                        </c:if>
                                    </c:forEach>
                                </a>
                            </div>
                            <div id="product_name">
                                <span>${product.getName()}</span>
                            </div>
                            <div id="product_description">
                                <span>${product.getDescription()}</span>
                            </div>
                            <div id="product_price">
                                <span>${product.getPrice()}</span>
                            </div>
                            <div id="empty"></div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div id="emptySearchResultsMsg">
                        <span>Ничего не найдено</span>
                    </div>
                </c:otherwise>
            </c:choose>

            <div id="searchResultsPaginationContainer">
                <ul id="searchPagesList">
                    <c:if test="${currentPage != 1 and currentPage > 0}">
                        <li class="page-item">
                            <a class="page-link" href="${basePath}/search?recordsPerPage=${recordsPerPage}&currentPage=${currentPage-1}&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">Previous</a>
                        </li>
                    </c:if>

                    <c:forEach begin="1" end="${numberOfPages}" var="i">
                        <c:choose>
                            <c:when test="${currentPage eq i}">
                                <li class="page-item active">
                                    <a class="page-link">
                                            ${i}
                                    </a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="page-item">
                                    <a class="page-link"href="${basePath}/search?recordsPerPage=${recordsPerPage}&currentPage=${i}&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">${i}</a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>

                    <c:if test="${currentPage lt numberOfPages}">
                        <li class="page-item">
                            <a class="page-link"href="${basePath}/search?recordsPerPage=${recordsPerPage}&currentPage=${currentPage+1}&searchKey=${searchKey}&categorySearchKey=${categorySearchKey}&priceFromKey=${priceFromKey}&priceToKey=${priceToKey}">Next</a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </body>
</html>
